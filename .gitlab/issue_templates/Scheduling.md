<!--

Borrowed liberally from https://gitlab.com/gitlab-org/distribution/team-tasks/blob/master/.gitlab/issue_templates/Scheduling.md

Set issue title as "Scheduling issue for the MAJOR.MINOR release" where MAJOR.MINOR is the GitLab version.

This template is used to create a release's scheduling issue. The scheduling issue
is used to raise visibility to the team on what issues are being looked at as
candidates for scheduling, and provides a place for them to offer up scheduling suggestions.

Engineering manager and Product manager are responsible for scheduling
work that is a direct responsibility of the Distribution team.

Typically the Engineering manager is the one to create this issue as part of their scheduling workflow

-->

### Current OKRs

<!--
List the current quarter's Memory Team OKRs
-->

[OKR Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1008667?label_name[]=Development%20Department) for reference

1. ...
2. ...
3. ...


### Team Availability

<!--

Use the Memory time-off calendar, and the the team meeting agenda doc to determine
what availability the team will have for the release.

We will use average availability to determine capacity.

- 1 backend engineer at 50%
- 1 backend engineers at 100%

Total 75% of backend engineer availability

-->

Total % of Team Member availability

### Priority and Severity boards

Priority Boards categorize the importance of all open issues, regardless of milestone. They are useful when looking for issue to consider for scheduling.

- [Memory:Workflow Kanban](https://gitlab.com/groups/gitlab-org/-/boards/1065668?label_name[]=Memory) - grouped by workflow state


### Scheduling boards

<!--

The Scheduling boards can be used to organize the issue candidates. The boards should be curated/scoped down by the Engineering Manager
and Product Manager during their regular meetings. Once the lists are agreed upon, the issues will get the milestone assigned

-->  

Scheduling boards are a work board used specifically during the scheduling process to give an overview of the candidates, and
used to get a idea of the next release before assigning the milestone to the issues.

- [Memory:Planning](https://gitlab.com/groups/gitlab-org/-/boards/1077383?label_name[]=Memory) - grouped by priority
- [Memory by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=Memory) grouped by milestone
- [Memory by Team Member](https://gitlab.com/groups/gitlab-org/-/boards/1156292?label_name[]=Memory)

### Status

1. [ ] Collect initial candidate issues
1. [ ] Issues added to the scheduling boards
1. [ ] Scheduling boards sorted/arranged by importance
1. [ ] Scheduling boards contain candidates from both Engineering and Product
1. [ ] Scheduling boards scoped to a reasonable amount of work for the release
1. [ ] Managers schedule the agreed issues for the release
